---
title: "Détails Formation"
author: ""
type: ""
date: 2022-09-28T15:38:17+02:00
subtitle: ""
image: ""
tags: [formation]
---

## Détails de la formation

Des précisions ont été apportées sur la formation Neo4J : date et contenu ont été précisés sur la [page dédiée](/page/formation).

Veuillez noter que la limite d'inscription pour suivre la formation a été avancée au 6 octobre.

Ne traînez pas pour pouvoir la suivre !
