---
title: "Contexte"
subtitle: ""
comments: false
date: 2022-04-26T18:36:13+02:00
lastmod: 2022-06-22T18:36:13+02:00
author: ""
authorLink: ""
description: ""
tags: []
categories: []
---

## Introduction

Le nombre de participants (>50) à chacun des deux _workshops_ {{< rawhtml >}}<a href="https://forum.dipso.inrae.fr/t/workshop-work4graphint-integration-de-donnees-en-graphe-les-9-et-10-decembre-2021-a-lisc-pf-a-paris-ouverture-des-inscriptions-et-soumissions/859" target="_blank">Work4graph</a>{{< /rawhtml >}}) dédiés à la  {{< rawhtml >}}<a href="https://sysmics.cati.inrae.fr/work4graph" target="_blank">visualisation</a>{{< /rawhtml >}} (2020), puis à {{< rawhtml >}}<a href="https://work4graph.pages.mia.inra.fr/work4graph-integration/" target="_blank">l’intégration</a>{{< /rawhtml >}} de données (2021) en graphe, financés par les SàPI DipSO/DSI[^1], a démontré un grand intérêt concernant l’usage des bases graphes pour la gestion de données hétérogènes et fortement connectées. Ces événements ont permis de mettre en lumière des initiatives locales peu coordonnées avec des niveaux d’avancement et de maturation diverses, de même que des besoins spécifiques à chaque communauté.
<!--more-->
Les actions issues du dernier _workshop_ ont permis d‘organiser un atelier découverte de l’écosystème Neo4j lors du séminaire CATI/PEPI/Pépinière numérique à Sète le 8 mars 2022, regroupant plus d’une vingtaine de participants, issus d’une dizaine de CATI différents. À cette occasion, des ingénieurs et chercheurs de Neo4j  ont présenté :

* des cas d’utilisation de Neo4j,
* des démonstrations de {{< rawhtml >}}<a href="https://github.com/neo4j-labs/neosemantics" target="_blank">Neosemantics</a>{{< /rawhtml >}}, un _plugin_ Neo4j permettant de faire le pont entre le monde RDF et les graphes de propriétés Neo4j,
* des démonstrations de la bibliothèque {{< rawhtml >}}<a href="https://neo4j.com/docs/graph-data-science/current/algorithms" target="_blank">Graph Data Science</a>{{< /rawhtml >}} (GDS) implémentant nativement des algorithmes de la théorie des graphes dans Neo4j,
* les outils de visualisation {{< rawhtml >}}<a href="https://neo4j.com/product/developer-tools/#browser" target="_blank">Neo4j Browser</a>{{< /rawhtml >}} et en particulier {{< rawhtml >}}<a href="https://neo4j.com/product/bloom" target="_blank">Bloom</a>{{< /rawhtml >}} permettant d’explorer un graphe Neo4j et de personnaliser la vue à destination d’utilisateurs non experts.

## Objectifs

Dans la continuité de ces présentations, avec le **soutien financier des SàPI DipSO/DSI 2022 et de la Formation Permanente Nationale**, nous proposons d’organiser un événement de type _bootcamp_/_hackathon_ pendant 3 jours pour une vingtaine de participants maximum, spécialisé sur la thématique de l’intégration et la visualisation de données en graphe. L’outillage autour de Neo4j et du RDF sera exploité, et le tout sera précédé d’une formation pour préparer au mieux les participants. Nous appuierons davantage sur l’un ou l’autre des points ci-dessous selon les profils et motivations des participants qui s’inscriront à l’événement. Les objectifs principaux sont de :

* partager les modélisations (génomiques, systémiques, ...) afin de converger vers des pivots d’interopérabilité permettant de lier plus facilement les graphes produits dans les différents groupes,
* exploiter les résultats d’analyses statistiques produits par certains groupes, pour intégrer les connaissances produites (au-delà des graphiques habituellement générés) avec les connaissances provenant d’autres graphes,
* partager et mettre en commun des portions de code permettant le traitement de fichiers de données biologiques et leur intégration sémantique, en explorant notamment des outils d’intégration développés en externe (ie. {{< rawhtml >}}<a href="https://github.com/monarch-initiative/dipper" target="_blank">dipper</a>{{< /rawhtml >}}, {{< rawhtml >}}<a href="https://d2s.semanticscience.org/" target="_blank">Data2Services</a>{{< /rawhtml >}}, ...),
* exploiter des outils d’analyses de graphes (ie. Neo4j Graph Data Sciences) afin d’inférer de nouvelles connaissances sur les graphes mis à disposition,
* développer des services d’interrogation et connecter des outils de visualisation (ie. {{< rawhtml >}}<a href="https://knetminer.com/" target="_blank">KnetMiner</a>{{< /rawhtml >}}/{{< rawhtml >}}<a href="https://knetminer.com/KnetMaps/)" target="_blank">KnetMaps</a>{{< /rawhtml >}}, Bloom, … sur les graphes générés, en exploitant les API de Neo4j,
* exploiter et comparer les capacités de raisonnement des plateformes (par exemple extraction d'information ou création de relations ou typage), avec ou sans ontologie.

Suite à l’atelier du séminaire à Sète, un intérêt certain pour Neo4j mais également un besoin de formation sur ces outils ont été identifiés. Afin que les participants du hackathon puissent tous se mettre à niveau et pour en renforcer les compétences dans l’Institut, nous avons pris contact avec les représentants de Neo4j afin de précéder le _bootcamp_ d’une session de formation. Les ingénieurs Neo4j seront également présents lors de l’événement pour assister les participants et les guider sur les modélisations, les bonnes pratiques et apporter leur expertise.

[^1]: Soutien à Projets structurants et Innovants de la DipSO et la DSI
