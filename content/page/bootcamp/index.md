---
title: "Bootcamp"
author: ""
type: ""
date: 2022-06-22T19:15:00+02:00
subtitle: ""
image: ""
tags: []
---

Le *bootcamp* permettra aux participants de s'impliquer sur les projets validés. Ils seront accompagnés et conseillés tout au long de l'événement par des ingénieurs de chez :
{{< rawhtml >}}
<img src="/neo4j-logo-2020-1.svg" height="50px" alt="Neo4j"/>
{{< /rawhtml >}}
<!--more-->

## Projets

### Soumissions

Si vous souhaitez proposer un nouveau projet, veuillez [suivre ces indications](/page/soumissions).

### Liste

Vous trouverez la liste des projets pressentis sur les *issues* de ce {{< rawhtml >}}<a href="https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/issues/" target="_blank">dépôt git</a>{{< /rawhtml >}}. Une fois maturés et validés, il seront référencés ici-même.

## Prérequis avant l'arrivée au *bootcamp*

Pour être une réussite, un *bootcamp* se prépare en amont. Ainsi d'un point de vue technique il vous faudra :

- avoir les idées claires du cas d'utilisation, si possible documenté et partagé en amont avec les équipes de Neo4j
- un environnement Neo4j fonctionnel pour un démarrage sur les chapeaux de roue
- disposer de données pertinentes pour votre *use-case*

C'est tout l'objet de la soumission de projet mentionnée ci-dessus. ;-)

## Hébergement

L'équipe d'organisation prévoit le financement des chambres d'hôtel pour les participants n'ayant pas de solution d'hébergement (notamment les non-franciliens).  
Vous pourrez solliciter une de ces chambres via le formulaire dès l'ouverture des inscriptions. Il vous faudra alors faire valider la réservation de l'hébergement **avant fin octobre** avec les instructions budgétaires que l'organisation vous fera parvenir.

***ATTENTION : La prise en charge des frais d'hébergement ne sera assurée que pour les inscriptions réalisées avant le 20 octobre 2022 !***

## Repas

Des paniers-repas seront fournis le lundi 21/11 midi à votre arrivée et le jeudi 24/11 midi pour le départ.

- Les petits-déjeuners seront pris en charge uniquement pour les participants bénéficiant de chambres réservées par l'organisation.
- Les déjeuners (pauses méridiennes) seront pris en charge par l'organisation.  
- Les dîners resteront cependant à votre charge.
