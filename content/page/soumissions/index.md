---
title: "Soumissions"
author: ""
type: ""
date: 2022-06-29T00:00:00+02:00
publishDate: 2022-06-29T00:00:00+02:00
expiryDate:  2022-10-31T23:59:00+02:00
subtitle: ""
image: ""
tags: []
---

## Soumission

Si vous souhaitez proposer un nouveau projet, veuillez suivre {{< rawhtml >}}<a href="https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/blob/main/README.md" target="_blank">les indications suivantes</a>{{< /rawhtml >}}. Il est recommandé de consolider votre projet en le discutant dans vos collectifs (CATI, unité, etc.) et au-delà, avec les autres participants potentiels, directement via les commentaires de votre page projet.
<!--more-->

Une fois votre projet structuré déposé et validé par l'équipe d'organisation, elle sera en mesure de l'ajouter sur le [site web de l'événement](/page/bootcamp), tout en se réservant le droit de le refuser (si le nombre final de projets déposés est trop important et/ou ne rassemble pas suffisamment de personnes).

En renseignant le formulaire adéquat, lors de l'inscription, les participants devront préciser le(s) projet(s) les intéressant, dans l'ordre de préférence.

Vous trouverez la liste des projets pressentis sur les *issues* de ce {{< rawhtml >}}<a href="https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/issues/" target="_blank">dépôt git</a>{{< /rawhtml >}}.

Nous insistons sur l'importance de définir des critères de réussite de chaque projet en amont du bootcamp, afin de :

- avoir une vision claire et partagée de l'objectif final
- pouvoir déterminer a l'issue du bootcamp si Neo4j est adapté ou pas aux cas d'usage

## Projets validés

Les 3 projets soumis ci-dessous ont été validés :

- [#1 - Intégration et export de données omiques et RDF via Neosemantics](https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/issues/1)
- [#2 - Visualisation de voies métaboliques](https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/issues/2)
- [#3 - Graph traversal](https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/issues/3)
