---
title: "Formation"
author: ""
type: ""
date: 2022-06-22T18:13:58+02:00
subtitle: ""
image: ""
tags: []
# draft: true
---

## Par qui ?

La formation, co-financée par la FPN[^1] et les SàPI[^2], sera dispensée par un ingénieur de chez :
{{< rawhtml >}}
<img src="/neo4j-logo-2020-1.svg" height="50px" alt="Neo4j"/>
{{< /rawhtml >}}

## Quand ?

L'inscription pour la formation est possible jusqu'au jeudi 6 octobre 2022 au plus tard.

La formation est prévue sur les 4 matinées suivantes de 8h30 à 12h30 :

- Session 1 (2x4h) : les **matinées** des **lundi 10 et mardi 11 octobre 2022**
- Session 2 (2x4h) : les **matinées** des **lundi 17 et mardi 18 octobre 2022**

## Comment ?

**En visio**, chaque participant aura besoin de disposer d'un ordinateur pour se connecter sur l'outil de visio et accéder à un environnement de travail Neo4j avec Neo4J Desktop, à télécharger sur le site <https://neo4j.com/download/>.

<!--more-->
## Quoi ?

La formation est constituée de 2 sessions de deux matinées chacune.

### Session 1

Cette session permettra aux débutants de prendre leurs marques avec les concepts autour de Neo4j :

- introduction aux graphes
  - comparaison avec les bases de données relationnelles
  - graphes de propriétés
  - introduction au {{< rawhtml >}}<a href="https://neo4j.com/developer/cypher/" target="_blank">langage Cypher</a>{{< /rawhtml >}}
- interrogations basiques avec Cypher
  - syntaxe Cypher, clauses MATCH, RETURN
  - clause WHERE
  - exploitation de *patterns* et des index
  - création et suppression de nœuds et de relations avec Cypher

### Session 2

Cette session permettra d'entrer davantage dans les détails et abordera les points suivants :

- import et gestion des données
  - fusion de données dans le graphe
  - définition de contraintes
  - aperçu des méthodes d'import de données
  - import avec LOAD CSV
  - import avec {{< rawhtml >}}<a href="https://neo4j.com/labs/apoc/" target="_blank">APOC (<em>Awesome Procedure On Graphs</em>)</a>{{< /rawhtml >}}
- modélisation de données en graphe
  - préparer un modèle graphe initial
  - *graph data modeling core principles*
  - structures de graphe principales
  - refactorer et faire évoluer un modèle de graphe

[^1]: Formation Permanente Nationale INRAE

[^2]: Soutiens À Projets structurants et Innovants 2022 de la DipSO et la DSI
