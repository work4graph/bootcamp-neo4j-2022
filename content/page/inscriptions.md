---
title: "Inscriptions"
author: ""
type: ""
date: 2022-09-05T00:00:00+02:00
publishDate: 2022-09-05T00:00:00+02:00
expiryDate:  2022-11-14T23:59:00+02:00
subtitle: ""
image: ""
tags: []
---

## Inscriptions ouvertes

Les inscriptions pour le *bootcamp* sont ouvertes du 5 septembre au 14 novembre 2022. Plus vous vous inscrirez tôt, plus grandes seront vos chances d'avoir une place parmi les 20 disponibles !  
Rappel : votre inscription sera prioritaire si vous participez à l'un des projets du *bootcamp*, *a fortiori* si vous en êtes  {{< rawhtml >}}<a href="https://forgemia.inra.fr/work4graph/propositions-bootcamp-neo4j/-/blob/main/README.md#propositions-bootcamp-neo4j" target="_blank">le porteur</a>{{< /rawhtml >}}.

Le formulaire d'inscription est disponible ici : {{< rawhtml >}}<a href="https://framaforms.org/inscription-bootcamp-work4graph-neo4j-1654879372" target="_blank">https://framaforms.org/inscription-bootcamp-work4graph-neo4j-1654879372</a>{{< /rawhtml >}}

***/!\ ATTENTION*** :

- les inscriptions pour la formation seront prises en compte jusqu'au **jeudi 6 octobre 2022**
- les inscriptions nécessitant une prise en charge des frais d'hébergement seront à réaliser jusqu'au **jeudi 20 octobre** dernier délai, afin que le financement puisse se faire sur le budget de l'année 2022.
