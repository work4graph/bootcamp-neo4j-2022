## En bref

### Quoi ?

Une **formation {{< rawhtml >}}<a href="https://neo4j.com/" target="_blank">Neo4j</a>{{< /rawhtml >}}** en amont d'un ***bootcamp***[^1] autour des graphes de connaissances exploitant la base de données graphe Neo4j.  
Avec des **ingénieurs Neo4j** pour nous **conseiller** et **accompagner** tout du long.

### Pour qui ?

Tout agent INRAE intéressé par les **graphes de connaissances**, les **modélisation**, **intégration** et **visualisation** de données, les **ontologies**, avec {{< rawhtml >}}<a href="https://neo4j.com/product/" target="_blank">l'écosystème Neo4j</a>{{< /rawhtml >}}.

### Quand ?

- **Soumissions** de projet : du 30 juin au 31 octobre 2022
- **Inscriptions** : du 5 septembre au 6 octobre pour la formation, jusqu'au 14 novembre 2022 pour le bootcamp
- **[Formation](/page/formation)** Neo4j : 4 matinées des 10, 11, 17 et 18 octobre, de 8h30 à 12h30
- ***[Bootcamp](/page/bootcamp)*** Neo4j : du lundi 21 novembre midi au jeudi 24 novembre 2022 midi

### Où ?

**Formation** : **en ligne** pour limiter les déplacements et simplifier votre logistique.  
***Bootcamp*** : **ISC-PIF**, 113 rue Nationale, 75013 Paris France {{< rawhtml >}}(<a href="https://www.openstreetmap.org/node/2503292092" target="_blank">OpenStreetMap</a>{{< /rawhtml >}} - {{<rawhtml >}}<a href="https://iscpif.fr/evenements/inrae-work4graph-bootcamp-neo4j/" target="_blank">Détails</a>{{< /rawhtml >}})

### Contact

Pour toute question ou remarque, n'hésitez pas :

- à nous retrouver sur le {{< rawhtml >}}<a href="https://forum.dipso.inrae.fr/search?expanded=false&q=bootcamp%20Neo4j" target="_blank">forum de la DipSO</a>{{< /rawhtml >}}
- à nous contacter par [e-mail](mailto:work4graph@inrae.fr?subject=[Bootcamp%20Neo4j%202022]%20Contact%20depuis%20le%20site%20web)

[^1]: certains disent aussi _hackathon_
